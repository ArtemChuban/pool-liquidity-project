# Pool Liquidity Project

## Plans
- [x] Graphic of gas difference old and new sqrt
![Benefits of realization new sqrt plot](./Benefits%20of%20realization%20optimized%20sqrt%20plot.png)
- Lookup table in sqrt function
```solidity
private mapping(uint256 => uint256) sqrts;
function sqrt(uint256 x) internal pure returns (uint256 result) {
    if (x <= 1) {
        return x;
    }
    if (sqrts[x] != 0) {
        return sqrts[x];
    }
    _;
    sqrts[x] = result;
    return result;
}
```

- [ ] Benefits of realization new sqrt
- [ ] Payable functions are cheaper
- [ ] Use indexed event params to reduce gas usage
- [ ] Revert with an error instead of using revert with strings
- [ ] Use separate checks in require() to save gas

## Proposals
- [Uniswap-v2 Improve Integer Square Root](https://github.com/Uniswap/v2-core/issues/201)
- [Uniswap-v2 Store dynamic bytes to call swap function with solidity assembly](https://github.com/Uniswap/v2-core/issues/188)
- [Uniswap-v2 change SLOAD to MLOAD](https://github.com/Uniswap/v2-core/pull/187)
- [Uniswap-v3 TickBitmap, SwapMath, Oracle: replace "if (A && B)" to save gas fees](https://github.com/Uniswap/v3-core/issues/656)
- [Uniswap-v3 Redundant code and optimization](https://github.com/Uniswap/v3-core/issues/613)
- [Uniswap-v3 move loop invariant variables out from loop to save gas](https://github.com/Uniswap/v3-core/pull/635)
- [Uniswap-v4 Use bit operation to perform modulo operations](https://github.com/Uniswap/v4-core/issues/265)
- [28 Ways to Optimize Gas Usage in Solidity Code](https://cryptoguide.dev/posts/solidity-gas-optimizations-guide/)
- [Uniswap V2 Audit Report](https://rskswap.com/audit.html)

## References
- [Improving front running resistance of x*y=k market makers](https://ethresear.ch/t/improving-front-running-resistance-of-x-y-k-market-makers/1281)
- [Formal Specification of Constant Product (x * y = k) Market Maker Model and Implementation](./references/x-y-k.pdf)
- [Uniswap-v2 Whitepaper](https://uniswap.org/whitepaper.pdf)
- [Github Core smart contracts of Uniswap V2](https://github.com/Uniswap/v2-core)
- [Uniswap-v3 Whitepaper](https://uniswap.org/whitepaper-v3.pdf)
- [Github Core smart contracts of Uniswap V3](https://github.com/Uniswap/v3-core)
- [Uniswap-v4 Whitepaper Draft](.references/whitepaper-v4-draft.pdf)
- [Github Core smart contracts of Uniswap V4](https://github.com/Uniswap/v4-core)
