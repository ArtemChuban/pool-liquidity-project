with open("txs.csv", "r") as file:
    data = file.read()
data = data.split('\n')
data = data[1:-1]

data = [x.split(',')[2] for x in data]
data = list(map(int, data))
gas_price = 69
gas_win = 10_000
res = sum(data) * gas_win * gas_price * 2 * 1e-9

print(f"{sum(data)} TXs")
print(f"{res} ETH")
